const Home = { template: '#p1' }

const Foo = { 
  template: '#p2',
  beforeRouteEnter (to, from, next) {
    vm.id_temp = 3699;
    next();
  },
}

const Bar = { 
  template: '#p3',
  created () {
    // fetch the data when the view is created and the data is
    // already being observed
    alert("created");
    this.fetchData();
  },
  methods:
  {
    fetchData () {
        alert("fetchdata")
        vm.id_temp = 11111;
        alert(vm.id_temp)
    }
  }
}


const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/foo', name: 'foo', component: Foo },
    { path: '/bar/:id', name: 'bar', component: Bar }
  ]
})

var vm = new Vue({
  router,
  data:
  {
      id_temp: 0
  },
  el: '#app'
}).$mount('#main')
