var requieren_login = {
    methods: {
        checkLogin() {
            axios.get(base_url + "ajax").then((result) => {
                this.userLoged = result.data.userLoged;
                this.userId = result.data.userId;
                this.userName = result.data.userName;
                this.userTipo = result.data.userTipo;
                this.registro_usuario.titular_usuario = result.data.userTitular;
                this.registro_usuario.descripcion_usuario = result.data.userDescripcion;
                this.file_foto_url = (result.data.userFoto != '') ? base_url + "/fotos_perfil/" + result.data.userFoto : result.data.userFoto;

                if (this.userLoged)
                    this.getDebatesFiltered('siguiendo', 0);
                else
                    this.getDebatesFiltered('recientes', 20);
            });
        },

        logout() {
            axios.get(base_url + 'ajax/logout').then((result) => {
                this.userLoged = false;
                this.userId = 0;
                this.userName = '';
                this.userEmail = '';
                this.userPass = '';
                this.es_propio_debate = false;
                this.getDebates();
                if (this.id_debate > 0)
                    this.getRespuestas(this.id_debate);
            });
        },

        putDebate() {
            axios.post(base_url + "ajax/addDebate", 
                { 
                    titulo: this.titulo_nuevo_debate, 
                    texto: this.texto_nuevo_debate,
                    temas: this.tags 
                }).then((result) => {
                this.debates.push(result.data);
                this.misDebates.push(result.data);
            });
        },

        borrarDebate(index) {
            axios.post(base_url + "ajax/deleteDebate", { valor: this.debates[index].id }).then((result) => {
                this.debates.splice(index, 1);
            });
        },

        clickPosicionate(index, posicion) {
            if (this.id_debate == 0) var id = this.debates[index].id;
            else var id = index;

            axios.post(base_url + "ajax/posicionarDebate", { id_debate: id, posicion: posicion }).then((result) => {
                if (this.id_debate == 0) this.debates[index].posicionamiento = posicion;
                else this.posicion_en_debate = posicion
            });
        },

        respondeDebate() {
            axios.post(base_url + "ajax/nuevaRespuesta", { su_debate: this.id_debate, texto: this.nueva_respuesta }).then((result) => {
                var respuesta = { id: result.data.id_respuesta, su_usuario: result.data.id_usuario, texto: this.nueva_respuesta };
                if (result.data.success) {
                    this['mis_opiniones'].push(respuesta);
                }

            });
        },

        seguimientoDebate(index, estado = true) {
            if (this.id_debate == 0)
                var id = this.debates[index].id;
            else
                var id = index;

            axios.post(base_url + "ajax/seguimientoDebate", { id_debate: id, estado: estado }).then((result) => {
                if (this.id_debate == 0)
                    if (estado == true)
                        this.debates[index].suscrito_debate = 1;
                    else
                        this.debates[index].suscrito_debate = 0;
                else
                // this.posicion_en_debate = posicion
                    console.log(id);

            });
        },

        completarDatosRegistro() {
            axios.post(base_url + "ajax/completarDatosRegistro", { id_usuario: this.userId, datos_registro: this.registro_usuario }).then((result) => {
                this.activeStep = result.data.step;
                this.userTipo = result.data.userTipo;
            });
        }
    }
}

var libres = {
    methods: {
        cambioFotoPerfil() {
            this.file_foto_url = URL.createObjectURL(this.file_foto);
        },

        actualizarDatosUsuario() {
            let formData = new FormData();
            formData.append('file', this.file_foto);
            formData.append('userTitular', this.registro_usuario.titular_usuario);
            formData.append('userDescripcion', this.registro_usuario.descripcion_usuario);

            axios.post(base_url + "ajax/datosUsuarioFoto",
                    formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                ).then(function(result) {
                    console.log('SUCCESS!!');
                })
                .catch(function(result) {
                    console.log('FAILURE!!');
                });
        },

        login() {
            axios.post(base_url + 'ajax/login', { user: this.userEmail, pass: this.userPass, acceso_libre: true }).then((result) => {
                if (result.data.userLoged == false) {
                    this.notificacionesActivas.errorLogin = true;
                }

                this.userLoged = result.data.userLoged;
                this.userId = result.data.userId;
                this.userName = result.data.userName;
                this.userTipo = result.data.userTipo;
                this.userEmail = '';
                this.userPass = '';
                this.registro_usuario.titular_usuario = result.data.userTitular;
                this.registro_usuario.descripcion_usuario = result.data.userDescripcion;
                this.file_foto_url = (result.data.userFoto != '') ? base_url + "fotos_perfil/" + result.data.userFoto : result.data.userFoto;

                if (this.seccion == 1) {
                    this.getRespuestas(this.id_debate);
                    this.getDebate(this.id_debate);
                } else if (this.seccion == 3) {
                    this.seccion = 0;
                }

                if (this.userLoged)
                    this.getDebatesFiltered('siguiendo', 0);
                else
                    this.getDebatesFiltered('recientes', 20);
            });
        },

        getTemasMasUsados() {
            var url = base_url + 'ajax/getTemasMasUsados';
            axios.post(url, { acceso_libre: true }).then((result) => {
                this.temas_usados = result.data;
            });
        },

        getTemas() {
            var url = base_url + 'ajax/getTemas';
            axios.post(url, { acceso_libre: true }).then((result) => {
                temas = result.data;
            });
        },

        getDebates() {
            var url = base_url + 'ajax/getDebates';
            axios.post(url, this.parametros).then((result) => {
                this.debates = result.data;
            });
        },

        getDebatesFiltered(modo, cantidad, valor_busqueda = 0, devolver = '') {
            // modo: "recientes", "mas_respuestas"
            // cantidad: int para "recientes"
            // devolver 0, poner en principal, 1 hacer return
            var titulo = '';
            this.parametros = {};
            this.parametros.acceso_libre = true;
            this.parametros.modo = modo;
            this.parametros.refresh_debates = true
            this.parametros.cantidad = cantidad;

            switch (modo) {
                case 'destacados':
                    titulo = "Debates Destacados";
                    break;
                case 'recientes':
                    titulo = "Últimos " + cantidad + " debates";
                    break;
                case 'ultimas_respuestas':
                    titulo = "Últimos debates repondidos";
                    break;
                case 'mas_respuestas':
                    titulo = "Debates con más respuestas";
                    break;
                case 'en_contra':
                    titulo = "Debates donde estoy en Contra";
                    break;
                case 'a_favor':
                    titulo = "Debates donde estoy a Favor";
                    break;
                case 'sin_posicionar':
                    titulo = "Debates donde aún no me he posicionado";
                    break;
                case 'siguiendo':
                    titulo = "Debates que estoy siguiendo";
                    break;
                case 'propios':
                    titulo = "Mis debates";
                    break;
                case 'busquedaTerminos':
                    this.parametros.terminoBusqueda = valor_busqueda;
                    titulo = "Debates con término: " + this.parametros.terminoBusqueda;
                    break;
                case 'entidad:autor':
                    this.parametros.id_entidad = valor_busqueda;
                    titulo = "Debates de " + this.parametros.id_entidad;
                    break;
                case 'entidad:tema':
                    this.parametros.id_entidad = valor_busqueda;
                    titulo = "Debates de Tema " + this.parametros.id_entidad;
                    break;
            }

            var url = base_url + 'ajax/getDebates';
            var resultado;

            axios.post(url, this.parametros).then((result) => {
                if (devolver != '') {
                    this[devolver] = result.data;
                } else {
                    this.notificacionesActivas.busquedaRealizada = true;
                    this.debates = result.data;
                    this.titulo_columna_debates = titulo;
                    VueScrollTo.scrollTo("#listaDebates");
                }
            });
        },

        getDebate(id) {
            axios.post(base_url + "ajax/getDebates", { id: id, acceso_libre: true }).then((result) => {
                this.posicion_en_debate = result.data.posicionamiento;
                this.es_propio_debate = (result.data.idAutor == this.userId);
            });
        },

        getRespuestas(id_debate) {
            axios.post(base_url + "ajax/getRespuestasDebate", { id_debate: id_debate, acceso_libre: true }).then((result) => {
                this.opiniones_favorables = result.data.a_favor;
                this.opiniones_negativas = result.data.en_contra;
                this.mis_opiniones = result.data.propias;
            });
        },

        clickDebate(index) {
            this.addHistoriaRutaFake(this.debates[index].titulo);
            this.id_debate = this.debates[index].id;
            this.posicion_en_debate = this.debates[index].posicionamiento;
            this.es_propio_debate = (this.debates[index].idAutor == this.userId);
            this.getRespuestas(this.id_debate);
            this.seccion = 1;
        },

        editTextoDebate() {
            this.editandoTextoDebate = true;
        },

        editTituloDebate() {
            this.editandoTituloDebate = true;
        },

        saveTextoDebate() {
            this.editandoTextoDebate = false;
            axios.post(base_url + "ajax/editDebate/", { id_debate: this.id_debate, campo: 'texto', valor: this.debateSeleccionado().texto }).then((result) => {
                // Nada
            });
        },

        saveTituloDebate() {
            this.editandoTituloDebate = false;
            axios.post(base_url + "ajax/editDebate", { id_debate: this.id_debate, campo: 'titulo', valor: this.debateSeleccionado().titulo }).then((result) => {
                // Nada
            });
        },

        addHistoriaRutaFake(ruta) {
            ruta = ruta.replace(/\s+/g, '-').toLowerCase();
            window.history.pushState({ href: ruta }, ruta, ruta);
        },
    }
}

// Global externo para que actuo de const, pero trayendo por ajax
var temas;

var vm = new Vue({
    el: '#main',
    mounted: function() {
        // Prueba Kendo (una vez cargado el ichero)
        kendo.culture("es-ES");
        this.checkLogin();
        this.getTemas();
        this.getTemasMasUsados();
        this.getDebatesFiltered('destacados', 5, 0, 'debatesLateral');
        this.addHistoriaRutaFake('');

        window.onpopstate = function(e) {
            if (e.state) {
                if (vm.seccion == 1) {
                    vm.seccion = 0;
                } else {
                    history.back();
                }
            }
        };

        VueScrollTo.setDefaults({
            container: "body",
            duration: 500,
            easing: "ease-out",
            offset: -15,
            force: false,
            cancelable: true,
            onStart: false,
            onDone: false,
            onCancel: false,
            x: false,
            y: true
        })
    },
    mixins: [requieren_login, libres],
    data: {
        file_foto: undefined,
        file_foto_url: '',
        registro_usuario: {
            errors: [],
            nombre: '',
            email: '',
            password: '',
            titular_usuario: '',
            descripcion_usuario: ''
        },
        temas_filtrados: [],
        tags: [],
        debates_loading: false,
        visibleHeaderMovil: true,
        notificacionesActivas: {
            errorLogin: false,
            errorRegistro: false,
            registroCorrecto: false,
            busquedaRealizada: false
        },
        activeTab: 1,
        userLoged: undefined,
        userTipo: 0,
        userEmail: '',
        userName: '',
        userPass: '',
        userId: 0,
        title: 'Debatium.com',
        id_debate: 0,
        posicion_en_debate: '',
        es_propio_debate: false,
        titulo_nuevo_debate: '',
        editandoTextoDebate: false,
        editandoTituloDebate: false,
        texto_nuevo_debate: '',
        nueva_respuesta: '',
        opiniones_favorables: [],
        opiniones_negativas: [],
        mis_opiniones: [],
        titulo_columna_debates: "Debates",
        parametros: { acceso_libre: true, refresh_debates: true, terminoBusqueda: '' },
        debates: [],
        misDebates: [],
        seccion: 0,
        showWeekNumber: false,
        locale: "locale",
        debatesLateral: [],
        activeStep: 0,
        componentKey: 0,
        componentKeyDS: 10,
        temas_usados: []
    },
    methods: {
        cambio_esquema: function() {
            this.schemaModelFields = {
                ProductID: { type: "number" },
                ProductName: { type: "string" },
                UnitPrice: { type: "number" },
                UnitsInStock: { type: "string" },
                Discontinued: { type: "string" }
            }
        },

        debateSeleccionado: function() {
            var debate_buscado = this.debates.find(function(debate, index) {
                if (debate.id == vm.id_debate)
                    return true;
            });
            return debate_buscado;
        },

        indexDebateSeleccionado: function() {
            var index_seleccionado = 0;
            this.debates.find(function(debate, index) {
                if (debate.id == vm.id_debate) {
                    index_seleccionado = index;
                }
            });
            return index_seleccionado;
        },

        getFilteredTags(text) {
            this.temas_filtrados = temas.filter((option) => {
                return option.nombre_tema
                    .toString()
                    .toLowerCase()
                    .indexOf(text.toLowerCase()) >= 0
            })
        },

        volverADebates() {
            this.getDebates();
            this.id_debate = 0;
            this.posicion_en_debate = '';
            this.es_propio_debate = false;
            this.editandoTextoDebate = false;
            this.editandoTituloDebate = false;
        },
        busquedaTerminos() {
            this.seccion = 0;
            this.getDebatesFiltered('busquedaTerminos', 0, this.parametros.terminoBusqueda)
        },
        sendRegistro: function(e) {
            this.registro_usuario.errors = [];

            if (this.registro_usuario.nombre == '') {
                this.registro_usuario.errors.push("El nombre es obligatorio.");
            }

            if (this.registro_usuario.email == '') {
                this.registro_usuario.errors.push('El correo electrónico es obligatorio.');
            } else if (!this.validEmail(this.registro_usuario.email)) {
                this.registro_usuario.errors.push('El correo electrónico debe ser válido.');
            }

            if (this.registro_usuario.password == '') {
                this.registro_usuario.errors.push("La contraseña es obligatoria.");
            }

            if (this.registro_usuario.errors.length > 0)
                this.notificacionesActivas.errorRegistro = true;
            else {
                this.notificacionesActivas.errorRegistro = false
                axios.post(base_url + "ajax/nuevoRegistro", { datos_registro: this.registro_usuario, acceso_libre: true }).then((result) => {

                    this.registro_usuario = result.data;
                    if (this.registro_usuario.errors.length > 0)
                        this.notificacionesActivas.errorRegistro = true;
                    else
                        this.notificacionesActivas.registroCorrecto = true;

                });
            }
        },
        validEmail: function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    },
    computed: { // Esto es solo una prueba, no vale, lo haría sobre los debates cargados

        /*
        misDebates: function () {
            return this.debates.filter((debate) => {
                return debate.idAutor == this.userId;
            });
        },
        */
        noHayDebates: function() {
            return this.debates.length == 0 && this.notificacionesActivas.busquedaRealizada;
        },
    },
    watch: {
        seccion: function(newSeccion, oldSeccion) {
            if (newSeccion == 0) {
                this.volverADebates();
            }

            if (newSeccion == 2) {
                this.getDebatesFiltered('propios', 0, 0, 'misDebates');
                if (this.userTipo == 10)
                    this.activeStep = 1;
                if (this.userTipo == 11)
                    this.activeStep = 2;
            }
        },

        schemaModelFields(val) {
            this.componentKeyDS += 1;
            this.componentKey += 1;
            //this.$refs.thegrid.kendoWidget().$forceUpdate();
            //this.$refs.remoteDataSource.kendoWidget().$forceUpdate();
            // console.log(this.$refs.remoteDataSource.kendoWidget())
        }
    },
});

// Add a request interceptor
axios.interceptors.request.use(function(config) {
    if ("data" in config && "refresh_debates" in config.data && config.data.refresh_debates == true) {
        vm.debates_loading = true;
    }

    NProgress.start();
    return config;
}, function(error) {
    console.log(error);
    return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use(function(response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    NProgress.done();
    vm.debates_loading = false;
    return response;
}, function(error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    console.log(error)
    return Promise.reject(error);
});