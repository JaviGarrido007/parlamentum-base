// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    NProgress.start();
    return config;
  }, function (error) {
    // Do something with request error
    console.log(error);
    return Promise.reject(error);
  }
);

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    NProgress.done();
    return response;
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    console.log(error)
    return Promise.reject(error);
  }
);

const ModalForm = {
        props: ['qr', 'url'],
        template: `
            <div class="modal-card" style="width: auto" >
                <header class="modal-card-foot">
                    <button class="button is-primary mx-2" type="button" @click="cerrarModal()">Cerrar</button>
                    <span class="title" v-html="url"></span>
                </header>
                <section class="modal-card-body columns is-desktop is-vcentered">
                    <b-loading v-model="qr_loading" :can-cancel="false"></b-loading>
                    <img class="column is-6 is-offset-one-quarter":src="qr" @load='qr_loading=false' /> 
                </section>
            </div>
        `,
        data() {
            return {
                qr_loading: true,
            };
        },
        methods:
        {
            cerrarModal: function () {
                parent.vm.getListaUrls();
                this.$parent.close();
            }
        }

}

var vm = new Vue({
  el: '#main',
  components: {
      ModalForm
  },
  mounted:function(){
      this.getListaUrls()
  },
  data: {
    isComponentModalActive: false,
    hasMobileCards: true,
    datosModal: {
        qr: '',
        url: ''
    },
    lista_urls: [],
    columnas_lista_urls: [
                {
                    field: 'id',
                    label: 'ID',
                    numeric: true
                },
                {
                    field: 'url',
                    label: 'URL',
                    searchable: true
                },
                {
                    field: 'shortCode',
                    label: 'ShortCode'
                },
                {
                    field: 'num_accesos',
                    label: 'Veces Vista',
                    subheading: 0,
                    numeric: true
                }
            ]
    
  },
  methods:
  {
      calcularSumaColumna: function(id_columna) {
          var valor = 0;
          this.lista_urls.forEach(function (item, index) {
            valor = valor + parseInt(item.num_accesos);
          });

          var i;
          this.columnas_lista_urls.forEach(function (item, index) {
              if (item.field == id_columna) {
                  i = index;
              }
          });

          this.columnas_lista_urls[i].subheading = valor;
      },

      cargarQrUrl: function (fila) {
          this.datosModal.qr = base_url + "SistemaQR/qr/" + fila.shortCode;
          this.datosModal.url = base_url + 'v/' + fila.shortCode + "<br><span style='font-size: 0.7em; font-weight:normal;'>" + fila.url + "</span>";
          this.isComponentModalActive = true;
      },

      getListaUrls: function () {
          var url = base_url + 'SistemaQR/getListaUrls';
          axios.post(url, { modo: 2 }).then((result) => {
              this.lista_urls = result.data;
              this.calcularSumaColumna('num_accesos');
          });
      },

      nuevaUrl: function (fila) {
            this.$buefy.dialog.prompt({
                message: "Introduce nueva URL completa",
                inputAttrs: {
                    maxlength: 200  
                },
                confirmText: 'Añadir',
                cancelText: 'Cancelar',
                trapFocus: true,
                onConfirm: (url) => 
                {
                  var destino = base_url + 'SistemaQR/addUrl';
                  axios.post(destino, { url: url }).then((result) => {
                      this.lista_urls.push(result.data);
                  });

                  this.$buefy.toast.open('Nueva Url ' + url);
                }

            })
      }
  }
});

