<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_debates extends CI_Model
{
	public $resultado;

	public function __construct()
	{
		parent::__construct();
		$this->resultado = array();

	}

	public function addDebate($titulo, $texto, $temas, $usuario)
	{
		$data = array("DEBA_Titulo" => $titulo,
					  "DEBA_Texto" => $texto,
					  "DEBA_Estado" => 1,
					  "DEBA_FechaCreacion" => date("Y-m-d H:i:s"),
					  "DEBA_SuCreador" => $usuario);

		$this->db->insert("debates", $data);
		$id_debate = $this->db->insert_id();

		foreach ($temas as $tema)
		{
			$data = array("TEDE_SuTema" => $tema['id_tema'],
						  "TEDE_SuDebate" => $id_debate);

			$this->db->insert("temas_debates", $data);
		}
		
		$this->resultado['success'] = true;
		$this->resultado['id_debate'] = $this->db->insert_id();

		return $this->resultado;
	}

	//////////////////////////////////////////////////////////////

	public function getDebates($id_usuario, $filtros = array(), $id = 0)
	{
		$select = "DEBA_Id as id, 
				   USUA_Nombre as autor, 
				   USUA_Id as idAutor, 
				   DEBA_Titulo as titulo, 
				   DEBA_Texto as texto, 
				   DEBA_FechaCreacion as fecha_creacion,
				   DEBA_Promocionado as destacado,
				   COUNT(todas.RESP_Id) as num_respuestas,
				   MAX(todas.RESP_Id) as ultima_respuesta,
				   (SELECT RESP_Texto from respuestas where RESP_Id = MAX(todas.RESP_Id)) as texto_ultima_respuesta,
				   IF(favor.posicionamiento_respuestas IS NULL, 
				   	  0, 
				   	  favor.posicionamiento_respuestas) as num_respuestas_favor,
				   IF(favor.posicionamiento_respuestas IS NULL, 
				   	  COUNT(todas.RESP_Id), 
				   	  COUNT(todas.RESP_Id) - IF(favor.posicionamiento_respuestas IS NULL, 0, favor.posicionamiento_respuestas)) as num_respuestas_contra,

				   JSON_ARRAYAGG(TEDE_SuTema) AS json_temas";

		if ($id_usuario > 0)
			$select .= ", MAX(posicionado_debates.PODE_Posicionamiento) as posicionamiento, 
						  (COUNT(suscritos_debates.SUDE_Id) > 0) as suscrito_debate";

						  
		if (isset($filtros['modo']) && $filtros['modo'] == "destacados")
		{
			$select .= ", 'https://picsum.photos/300/200' as imagenFondo";
		}

		$this->db->select($select);
		$this->db->from("debates");

		if ($id > 0)
			$this->db->where("DEBA_id", $id);

		if (isset($filtros['modo'])) 
		{
			if ($id_usuario > 0 && $filtros['modo'] == "en_contra") {
				$this->db->where("posicionado_debates.PODE_Posicionamiento", "C");
			}

			if ($id_usuario > 0 && $filtros['modo'] == "a_favor") {
				$this->db->where("posicionado_debates.PODE_Posicionamiento", "F");
			}

			if ($id_usuario > 0 && $filtros['modo'] == "sin_posicionar") {
				$this->db->where("posicionado_debates.PODE_Posicionamiento", NULL);
			}

			if ($id_usuario > 0 && $filtros['modo'] == "siguiendo") {
				$this->db->having('COUNT(suscritos_debates.SUDE_Id) >', 0);  
			}

			if ($id_usuario > 0 && $filtros['modo'] == "propios") {
				$this->db->where('DEBA_SuCreador', $id_usuario);  
			}

			if ($filtros['modo'] == "entidad:autor") {
				$this->db->where('DEBA_SuCreador', $filtros['id_entidad']);  
			}


			if ($filtros['modo'] == "entidad:tema") {
				$this->db->having("JSON_CONTAINS(json_temas, '"  . $filtros['id_entidad'] . "')");  
			}

			if ($filtros['modo'] == "busquedaTerminos") {
				$this->db->like('DEBA_Titulo', $filtros['terminoBusqueda']);  
			}

			if ($filtros['modo'] == "destacados") {
				$this->db->where('DEBA_Promocionado', true);  
			}
		}

		$this->db->join("usuarios", "debates.DEBA_SuCreador = usuarios.USUA_Id");
		
		$this->db->join("respuestas as todas", "debates.DEBA_Id = todas.RESP_SuDebate", "LEFT");

		$this->db->join("(select COUNT(PODE_Posicionamiento) as posicionamiento_respuestas, RESP_SuDebate
						  from respuestas join posicionado_debates 
						  ON posicionado_debates.PODE_SuUsuario = respuestas.RESP_SuUsuario AND posicionado_debates.PODE_SuDebate = respuestas.RESP_SuDebate
						  where PODE_Posicionamiento = 'F'
						  GROUP BY(posicionado_debates.PODE_SuDebate)) as favor", "debates.DEBA_Id = favor.RESP_SuDebate", "LEFT");

		//if (isset($filtros['modo'])) {
			//if ($filtros['modo'] == "entidad:tema") {
				$this->db->join("temas_debates as td", "debates.DEBA_Id = td.TEDE_SuDebate", "LEFT");  
			//}
		//}
 
		if ($id_usuario > 0)
			$this->db->join('posicionado_debates', 'posicionado_debates.PODE_SuUsuario = ' . $id_usuario . ' AND posicionado_debates.PODE_SuDebate = debates.DEBA_Id', 'LEFT');

		if ($id_usuario > 0)
			$this->db->join('suscritos_debates', 'suscritos_debates.SUDE_SuUsuario = ' . $id_usuario . ' AND suscritos_debates.SUDE_SuDebate = debates.DEBA_Id', 'LEFT');

		$this->db->group_by("DEBA_Id");

		if (isset($filtros['modo'])) {

			if ($filtros['modo'] == "recientes") {
				$this->db->order_by("DEBA_FechaCreacion", "DESC");
				$this->db->limit($filtros['cantidad']);
			}

			if ($filtros['modo'] == "mas_respuestas") {
				$this->db->order_by("num_respuestas", "DESC");
			}

			if ($filtros['modo'] == "ultimas_respuestas") {
				$this->db->order_by("ultima_respuesta", "DESC");
			}		
		}
		else
		{
			$this->db->order_by("DEBA_Id", "ASC");
		}

		$query = $this->db->get();

		//echo $this->db->last_query();

		if ($id > 0) {
			return $query->row_array();
		}
		else {
			return $query->result_array();
		}
	}

	///////////////////////////////////////////////////////////////////

}