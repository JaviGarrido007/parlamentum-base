<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_acceso extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		session_start();
	}

	public function isUserLoged()
	{
		return (isset($_SESSION['id_usuario']) && $_SESSION['id_usuario'] > 0) ? true: false;
	}

	public function getUserId()
	{
		return $_SESSION['id_usuario'];
	}

	public function getDatosUsuario($id)
	{
		$this->db->select("USUA_Id as id, USUA_Nombre as nombre, USUA_Tipo as tipo, 
						   USUA_Titular as titular, USUA_Descripcion as descripcion, 
						   USUA_Foto as foto_perfil");
		
		$this->db->from("usuarios");
		$this->db->where("USUA_Id", $id);
		
		$query = $this->db->get();

		if ($query->num_rows() == 1)
			return $query->row_array();
		else
			return array("id" => 0);
	}

	public function identificar($user, $pass)
	{
		$this->db->select("USUA_Id as id, USUA_Nombre as nombre, USUA_Tipo as tipo, USUA_Titular as titular, USUA_Descripcion as descripcion, USUA_Foto as foto_perfil");
		$this->db->from("usuarios");
		$this->db->where("USUA_Pass", md5($pass))
		->group_start()
		->where("USUA_Email", $user)
		->or_where("USUA_Nombre", $user)
		->group_end();

		$query = $this->db->get();

		if ($query->num_rows() == 1)
			return $query->row_array();
		else
			return array("id" => 0);
	}

	public function putUserId($id)
	{
		$_SESSION['id_usuario'] = $id;
	}

	public function logout()
	{
		$_SESSION['id_usuario'] = 0;
	}

}