<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_utilidades extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function subir_fichero($form_fichero, $directorio, $tipos)
	{
		$config_upload['upload_path'] = $directorio.'/';
		$config_upload['allowed_types'] = $tipos;
		$config_upload['max_size']	= '4096';
		$config_upload['encrypt_name'] = true;

		$this->load->library('upload', $config_upload);
		$this->upload->initialize($config_upload);

		if (!$this->upload->do_upload($form_fichero))
		{
			return array('fichero'=> -1, 'error'=>strip_tags($this->upload->display_errors()), "msg"=>"No se ha podido subir el fichero");
		}
		else
		{
			//Recojo los datos del archivo subido
			$datos_foto = $this->upload->data();

	        //Thumbnail Image Upload - Start
	        $config['image_library'] = 'gd2';
	        $config['source_image'] = $datos_foto['full_path'];
	        $config['new_image'] = $datos_foto['file_path'] . "thumb_" . $datos_foto['file_name'];
	        $config['create_thumb'] = TRUE;
	        $config['thumb_marker'] = "";
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 75;
			$config['height'] = 50;

	        // load resize library
	        $this->load->library('image_lib', $config);
	        $this->image_lib->initialize($config);

	        if(! $this->image_lib->resize())
	        {
	        	echo $this->image_lib->display_errors();
	        }
	       
			return array('fichero' => $datos_foto, 'error'=>"");
		}
	}
}