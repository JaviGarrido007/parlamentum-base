<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_inits extends CI_Model
{
	public $resultado;

	public function __construct()
	{
		parent::__construct();
		$this->load->dbforge();
	}

	public function dbInit()
	{
		$this->db->db_debug = false;

		$this->dbforge->add_column("debates", array(
		        'DEBA_Titulo' => array(
		        	'type' => 'TEXT', 
		        	'after' => 'DEBA_Estado'
		        )
		    )
		);	

		$this->db->set('DEBA_Titulo', 'DEBA_Texto', FALSE);
		$this->db->where('DEBA_Titulo', NULL);
		$this->db->or_where('DEBA_Titulo', '');
		$this->db->update('debates');


		$this->dbforge->add_key('ACQR_Id', TRUE);
		$fields = array(
		        'ACQR_Id' => array(
		                'type' => 'INT',
		                'auto_increment' => TRUE
		        ),
		        'ACQR_Url' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '250'
		        ),
		        'ACQR_NumAccesos' => array(
		                'type' =>'INT'
		        )
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->create_table('accesos_qr', true);

		$this->dbforge->add_column("debates", array(
		        'DEBA_Promocionado' => array(
		        	'type' => 'BOOL'
		        )
		    )
		);

		$fields = array(
        'DEBA_FechaCreacion' => array(
                'name' => 'DEBA_FechaCreacion',
                'type' => 'DATETIME',
        ));

        $this->dbforge->modify_column('debates', $fields);

        $this->dbforge->add_column("accesos_qr", array(
		        'ACQR_CodigoShort' => array(
		        	'type' => 'VARCHAR',
		            'constraint' => '10'
		        )
		    )
		);
        

        $this->dbforge->add_key('RECU_Id', TRUE);
		$fields = array(
		        'RECU_Id' => array(
		                'type' => 'INT',
		                'auto_increment' => TRUE
		        ),
		        'RECU_Valor' => array(
		                'type' => 'TEXT'
		        ),
		        'RECU_Tipo' => array(
		                'type' => 'ENUM("IMAGEN","URL","TEXTO")',
						'default' => "TEXTO",
						'null' => FALSE,
		        )
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->create_table('recursos', true);


		$this->db->db_debug = true;
	}

}