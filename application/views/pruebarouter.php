<style>
	.view{
		width: 50%;
		margin: auto;
	}
	.component-fade-enter-active, .component-fade-leave-active {
	  transition: opacity .3s ease;
	}
	.component-fade-enter, .component-fade-leave-to
	/* .component-fade-leave-active below version 2.1.8 */ {
	  opacity: 0;
	}
</style>
<div id="main"></div>

<div id="app">
  <h1>Named Routes</h1>
  <p>Current route name: {{ $route.name }} {{ $route.seccion }}</p>
  <ul>
    <li><router-link :to="{ name: 'home' }">home</router-link></li>
    <li><router-link :to="{ name: 'foo' , params: { id: id_temp }}">foo</router-link></li>
    <li><router-link :to="{ name: 'bar', params: { id: id_temp }}">bar</router-link></li>
    <li><a href="http://192.168.1.38/javiVue/parlamentum-base/?permitido=1">Debatium</a></li>
  </ul>
  <transition name="component-fade" mode="out-in">
    	<router-view class="view"></router-view>
  </transition>
</div>

<template id="p1">
	<div>This is Home</div>
</template>

<template id="p2">
	<div>This is Foo {{ $parent.id_temp }}, {{ $route.params.id }} </div>
</template>

<template id="p3">
	<div>This is Bar {{ $parent.id_temp }}</div>
</template>
