<div id="main" v-cloak>
	<div class="container py-5">
		<b-field grouped group-multiline>
			<div class="control">
				<b-button type="is-primary" @click="nuevaUrl">Nueva Url</b-button>
			</div>
			<div class="control is-flex">
				<b-switch v-model="hasMobileCards">Usar Fórmato Cards en Móvil</b-switch>
			</div>
		</b-field>
		<template>
		    <b-table @click="cargarQrUrl"
		    	:data="lista_urls" :columns="columnas_lista_urls"
		    	:mobile-cards="hasMobileCards">
			</b-table>
		</template>
		<b-modal 
            v-model="isComponentModalActive"
            has-modal-card
            full-screen 
            :can-cancel="false">
            <modal-form v-bind="datosModal"></modal-form>
        </b-modal>
	</div>
</div>