<div id="main" v-cloak>
	<div class="container py-5">
		<template>
		    <nav class="level navbar">
			  <!-- Left side -->
			  <div class="level-left" v-show="visibleHeaderMovil">
				    <div class="level-item">
				    	<figure class="image">
					  		<img class="is-clickable" @click.stop.prevent="seccion=0" src="<?php echo base_url();?>assets/imagenes/logo_debatium.png" />
						</figure>
				    </div>

				    <div class="level-item ml-2">
						<b-field label="Busca por términos" label-position="on-border">
				            <b-input type="search" v-model="parametros.terminoBusqueda"></b-input>
				            <p class="control">
				                <b-button class="button is-primary" @click.stop.prevent="busquedaTerminos()">Buscar</b-button>
				            </p>
				        </b-field>
				    </div>

			  </div>
			  <!-- Right side -->
			  <div class="level-right" v-if="userLoged">
			  		<p class="level-item" v-show="visibleHeaderMovil">
			  			<span class="subtitle is-6 is-family-secondary has-text-grey-dark has-text-weight-semibold is-italic">Bienvenido, {{ userName }}</span>
			  		</p>

			    	<p class="level-item">
			    		<template>
						    <b-dropdown v-model="seccion" aria-role="list">
						        <button class="button is-primary" type="button" slot="trigger">
						            <template v-if="seccion==0">
						                <b-icon icon="earth"></b-icon>
						                <span>Debates</span>
						            </template>
						            <template v-if="seccion==1">
						                <b-icon icon="earth"></b-icon>
						                <span>Info debate</span>
						            </template>
						            <template v-if="seccion==2">
						                <b-icon icon="account-multiple"></b-icon>
						                <span>Mi Cuenta</span>
						            </template>
						            <b-icon icon="menu-down"></b-icon>
						        </button>

						        <b-dropdown-item :value="0" aria-role="listitem">
						            <div class="media">
						                <b-icon class="media-left" icon="earth"></b-icon>
						                <div class="media-content">
						                    <h3>Debates</h3>
						                </div>
						            </div>
						        </b-dropdown-item>

						        <b-dropdown-item :value="2" aria-role="listitem">
						            <div class="media">
						                <b-icon class="media-left" icon="account-multiple"></b-icon>
						                <div class="media-content">
						                    <h3>Mi Cuenta</h3>
						                </div>
						            </div>
						        </b-dropdown-item>
						    </b-dropdown>
						</template>

						<b-button class="button is-primary is-light ml-3" icon-right="logout-variant"  @click.stop.prevent="logout()">Logout</b-button>

			    		<b-button class="button is-primary is-light ml-3 is-hidden-tablet" 
			    				  :icon-left="visibleHeaderMovil ? 'chevron-double-up' : 'chevron-double-down'"
			    				  @click.stop.prevent="visibleHeaderMovil=!visibleHeaderMovil" rounded> </b-button>
			    	</p>
			  </div>
			  <div class="level-right is-mobile" v-if="userLoged==false">
			  	<div class="level-item">
				  	<b-button @click.stop.prevent="seccion=3"
	                    class="is-primary mr-2"
	                    icon-right="pencil-plus">
	                    Regístrate
	                </b-button>

				  	<b-dropdown position="is-bottom-left" append-to-body aria-role="menu" trap-focus>
	                    <b-button
	                        class="is-primary"
	                        slot="trigger"
	                        role="button"
	                        icon-right="menu-down">
	                        Iniciar Sesión
	                    </b-button>
	                    <b-dropdown-item
	                        aria-role="menu-item"
	                        :focusable="false"
	                        custom
	                        paddingless>
	                        <div class="columns is-mobile is-centered" style="margin: auto;">
	                            <div class="modal-card column is-mobile" style="width: 20rem;">
	                                <section class="modal-card-body">
	                                    <b-field label="Usuario o Email" label-position="on-border">
	                                        <b-input
	                                        	v-model="userEmail"
	                                            type="text"
	                                            placeholder=""
	                                            required>
	                                        </b-input>
	                                    </b-field>

	                                    <b-field label="Contraseña" label-position="on-border">
	                                        <b-input
	                                        	v-model="userPass"
	                                            type="password"
	                                            password-reveal
	                                            placeholder=""
	                                            required>
	                                        </b-input>
	                                    </b-field>

	                                    <b-notification
	                                    	class="mb-1"
								            auto-close type="is-danger is-light"
								            :duration="3500"
								            v-model="notificacionesActivas.errorLogin">
								            Los datos introducidos son incorrectos
								        </b-notification>

	                                    <button @click.stop.prevent="login()" class="button is-primary mt-3">Iniciar Sesión</button>
	                                </section>
	                            </div>
	                        </div>
	                    </b-dropdown-item>
	                </b-dropdown>
            		</div>
			  </div>
			</nav>
		</template>

		<div class="columns">
		    
		    <div class="column is-9 is-paddingless mb-5" v-if="seccion === 0">
		    	<div class="panel panel-default is-primary" style="position: relative;">
			    	<b-loading v-model="debates_loading" :can-cancel="false" :is-full-page="false"></b-loading>
			    	<div class="panel-heading">
			    		<h3 id="listaDebates"class="panel-title">{{ titulo_columna_debates }}</h3>
			  		</div>
		  			<b-notification
			            type="is-warning"
			            :active="noHayDebates">
			            No hay debates que cumplan esos criterios
			        </b-notification>
		  			<article  v-for="(debate, index) in debates" class="media px-3 py-3" :class="debate.destacado == true ? 'destacado' : ''">
					  	<figure class="media-left mt-4">
						    <div v-if="debate.posicionamiento !== null">
		    					<b-icon v-if="debate.posicionamiento == 'F'" 
					                icon="thumb-up"
					                size="is-medium"
					                type="is-success">
					            </b-icon>
					            <b-icon v-if="debate.posicionamiento == 'C'"
					                icon="thumb-down"
					                size="is-medium"
					                type="is-danger">
					            </b-icon>
		    				</div>
		    				<div v-else="">
		    					<b-icon
					                icon="help"
					                size="is-medium"
					                type="is-warning">
					            </b-icon>
		    				</div>
					  	</figure>

					  	<div class="media-content">
						    <div class="content">
						    	<div class="level level-is-shrinkable" @click.stop.prevent="clickDebate(index)">
							      <div class="level-left">
							      		<div class="level-item">
							      			<strong>{{ debate.titulo }}</strong>
							      		</div>
							      </div>
							      <div class="level-right px-1">
							      		<div class="level-item is-size-7">
							      			<b-tag type="is-info is-light" class="is-clickable" rounded
							      			       @click.native.stop.prevent="getDebatesFiltered('entidad:autor', 0, debate.idAutor)">
							      				{{ debate.autor }}
							      			</b-tag>
							      			<span class="mx-2">({{ new Date(debate.fecha_creacion).toLocaleDateString() }})</span>
							      			<b-icon class="mx-2"
								                icon="thumb-up"
								                size="is-small"
								                type="is-success">
								            </b-icon>
								            {{ debate.num_respuestas_favor }}
								            <b-icon class="mx-2"
								                icon="thumb-down"
								                size="is-small"
								                type="is-danger">
								            </b-icon>
								            {{ debate.num_respuestas_contra }}
							      		</div>
							      </div>
							    </div>
						        {{ debate.texto }}
						    </div>

				    		<nav class="is-mobile level">
						    	<div class="level-left">
							        <div class="level-item">
							        	<b-button type="is-text" class="is-small" outlined
							        		v-if="debate.suscrito_debate == 0 && userLoged"
							                icon-left="bookmark-plus-outline"  
							                @click.stop.prevent="seguimientoDebate(index, true)"> 
							                Siguelo
							            </b-button>
							            <span v-if="debate.suscrito_debate == 1 && userLoged">
							            	<div class="field">
							            		<b-tag
							            			type='is-warning is-light'
									                close-type='is-warning'
									                close-icon-type='is-grey'
									                closable
									                close-icon='close'
									                @close="seguimientoDebate(index, false)"
									                rounded>
									                Siguiendo &nbsp;&nbsp;
									            </b-tag>
									        </div>
							            </span>
							        </div>

							        <div class="level-item">
							        	<b-tag class="mr-2 is-clickable" v-if="tema!=null" v-for="(tema, index) in JSON.parse(debate.json_temas)" 
							        		   type="is-primary" rounded
						      			       @click.native.stop.prevent="getDebatesFiltered('entidad:tema', 0, tema)">
						      				{{ tema }}
						      			</b-tag>

							        </div>
		      					</div>
						    	<div class="level-right" v-if="debate.posicionamiento == null && userLoged">
							        <div class="level-item">
							          	<b-button type="is-success" class="is-small mr-2" outlined
						                	icon-left="thumb-up"  
						                	@click.stop.prevent="clickPosicionate(index, 'F')">
						                	A Favor
						            	</b-button>
							         	 <b-button type="is-danger" class="is-small" outlined
							                icon-left="thumb-down"  
							                @click.stop.prevent="clickPosicionate(index, 'C')"> 
							                En Contra
							            </b-button>
							        </div>
							    </div>
				    		</nav>
				  		</div>
					</article>	
		    	</div>
		    </div>

		    <div class="column is-3 pt-0" v-if="seccion === 0">
				<template>
				    <b-carousel-list :data="debatesLateral" :items-to-show="1">
				        <template slot="item" slot-scope="debate">
				            <div class="card">
				                <div class="card-image" imagenFondo>
					                <div class="imgFondoLateral hero-body has-text-centered has-background-grey-lighter" 
					                	 v-bind:style="{ backgroundImage: 'url(' + debate.imagenFondo + ')' }">
					                    <h1 class="title is-5 has-text-white">{{ debate.titulo }}</h1>
					                </div>
					                <b-tag style="position: absolute; top: 5px; left: 5px"
						                type="is-primary"
						                rounded>
						                {{ debate.num_respuestas }} Respuestas
						            </b-tag>
				                </div>
				                <div class="card-content">
				                    <div class="content">
				                        <p class="title is-6">{{ debate.autor }}</p>
				                        <p class="subtitle is-7">{{ debate.fecha_creacion }}</p>
				                        <div class="field is-grouped" >
				                            <p class="control" style="margin-left: auto">
				                                <b-button type="is-primary" class="is-small" outlined
									        		v-if="debate.suscrito_debate == 0 && userLoged"
									                icon-left="bookmark-plus-outline"  
									                @click.stop.prevent="seguirDebate(index)"> 
									                Siguelo
									            </b-button>
									            <div class="field" v-if="debate.suscrito_debate == 1 && userLoged">
											        <b-tag rounded>Siguiendo</b-tag>
											    </div>
				                            </p>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </template>
				    </b-carousel-list>
				</template>

				<b-button type="is-link" class="is-small my-2" outlined expanded
	                icon-left="bookmark-plus-outline"  
	                @click.stop.prevent="getDebatesFiltered('recientes', 5)"> 
	                5 Más recientes
	            </b-button>
	            <b-button type="is-info" class="is-small my-2" outlined expanded
	                icon-left="bookmark-plus-outline"  
	                @click.stop.prevent="getDebatesFiltered('mas_respuestas', 0)"> 
	                Con más respuestas
	            </b-button>
	            <b-button type="is-dark" class="is-small my-2" outlined expanded
	                icon-left="bookmark-plus-outline"  
	                @click.stop.prevent="getDebatesFiltered('ultimas_respuestas', 0)"> 
	                Debates con interacciones más recientes
	            </b-button>
	            <b-button type="is-dark" class="is-small my-2" outlined expanded
	                icon-left="bookmark-plus-outline"  
	                @click.stop.prevent="getDebatesFiltered('destacados', 0)"> 
	                Debates Destacados
	            </b-button>

	            <template v-if="userLoged">
		            <b-button type="is-danger" class="is-small my-2" outlined expanded
		                icon-left="bookmark-plus-outline"  
		                @click.stop.prevent="getDebatesFiltered('en_contra', 0)"> 
		                Estoy en Contra
		            </b-button>
		            <b-button type="is-success" class="is-small my-2" outlined expanded
		                icon-left="bookmark-plus-outline"  
		                @click.stop.prevent="getDebatesFiltered('a_favor', 0)"> 
		                Estoy a Favor
		            </b-button>
		            <b-button type="is-warning" class="is-small my-2" outlined expanded
		                icon-left="bookmark-plus-outline"  
		                @click.stop.prevent="getDebatesFiltered('sin_posicionar', 0)"> 
		                Sin Posicionar
		            </b-button>
		            <b-button type="is-text" class="is-small my-2" outlined expanded
		                icon-left="bookmark-plus-outline"  
		                @click.stop.prevent="getDebatesFiltered('siguiendo', 0)"> 
		                Siguiendo
		            </b-button>
		            <b-button type="is-info" class="is-small my-2" outlined expanded
		                icon-left="bookmark-plus-outline"
		                @click.stop.prevent="getDebatesFiltered('propios', 0)"> 
		                Mis Debates
		            </b-button>
		        </template>

		        <template>
		        	<ul class="list-group">
		    			<li class="list-group-item" v-for="tema in temas_usados">
		    				<b-tag type="is-primary" class="is-clickable" rounded
			      			       @click.native.stop.prevent="getDebatesFiltered('entidad:tema', 0, tema.idTema)">
			      				({{ tema.veces_usado_tema }}) {{ tema.nombre_tema }}
			      			</b-tag>
		    			</li>
			    	</ul>
			    </template>
		    </div>

		    <div class="column is-hidden-mobile" v-if="seccion === 1">
		    	<h1>A Favor</h1>
		      	<ul class="list-group">
	    			<li class="list-group-item" v-for="opinion_favorable in opiniones_favorables">
	    				{{ '[' + opinion_favorable.id + '] ' + opinion_favorable.su_usuario + ' - ' + opinion_favorable.texto }}
	    			</li>
		    	</ul>
		    </div>

		    <div class="column is-hidden-mobile" v-if="seccion === 1">
		    	<b-input type="textarea" v-if="editandoTituloDebate && es_propio_debate" @keydown.native.enter="saveTituloDebate()" v-model="debateSeleccionado().titulo"></b-input>
		    	<h1 class="title is-4" v-else="" @click.stop.prevent="es_propio_debate ? editTituloDebate() : null">{{  debateSeleccionado().titulo  }}</h1>

		    	<b-input type="textarea" v-if="editandoTextoDebate && es_propio_debate" @keydown.native.enter="saveTextoDebate()" v-model="debateSeleccionado().texto"></b-input>
		    	<span v-else="" @click.stop.prevent="es_propio_debate ? editTextoDebate() : null">{{  debateSeleccionado().texto  }}</span>

		    	<template v-if="userLoged && posicion_en_debate != null">
		    		<h1>Posicionado: {{ posicion_en_debate }}</h1>
			    	<h1>Mis Opiniones</h1>
			      	<ul class="list-group">
		    			<li class="list-group-item" v-for="opinion in mis_opiniones">
		    				<span>{{  opinion.id }}</span> -  
		    				<span>{{  opinion.texto }}</span>
		    			</li>
			    	</ul>
			    
			    	<h1>Responde:</h1>
			    	<b-input type="textarea" v-model="nueva_respuesta"></b-input>
			    	<br>
			    	<button class="btn btn-success" @click.stop.prevent="respondeDebate()">Responde Debate</button>
			    </template>
			    <template v-else-if="userLoged">
			    	<h1>Posicionate</h1>
			    	<b-button type="is-success" outlined @click.stop.prevent="clickPosicionate(id_debate, 'F')">A Favor</b-button>
	    			<b-button type="is-danger" outlined @click.stop.prevent="clickPosicionate(id_debate, 'C')">En Contra</b-button>
	    		</template>
		    </div>

		    <div class="column is-hidden-mobile" v-if="seccion === 1">
		      	<h1>En Contra</h1>
		      	<ul class="list-group">
	    			<li class="list-group-item" v-for="opinion_negativa in opiniones_negativas">
	    				{{ '[' + opinion_negativa.id + '] ' + opinion_negativa.su_usuario + ' - ' + opinion_negativa.texto }}
	    			</li>
		    	</ul>
		    </div>

		    <div class="column is-hidden-tablet" v-if="seccion === 1">
		    	<template>
				    <b-tabs v-model="activeTab"
				    		size="is-small" 
				    		position="is-centered" 
				    		class="mt-0" 
				    		style="overflow: hidden;" 
				    		expanded
				    		type="is-boxed">
				        <b-tab-item>
				            <template slot="header">
				                <b-icon icon="thumb-down" type="is-danger" size="is-small"></b-icon>
				                <span>En Contra <b-tag size="is-small" rounded> {{ opiniones_negativas.length }} </b-tag> </span>
				            </template>

				            <h1>En Contra</h1>
					      	<ul class="list-group">
				    			<li class="list-group-item" v-for="opinion_negativa in opiniones_negativas">
				    				{{ '[' + opinion_negativa.id + '] ' + opinion_negativa.su_usuario + ' - ' + opinion_negativa.texto }}
				    			</li>
					    	</ul>
					    </b-tab-item>

				        <b-tab-item>
				        	<template slot="header">
				        		<b-icon v-if="userLoged"
				        			:icon="posicion_en_debate != null ? (posicion_en_debate == 'F' ? 'thumb-up' : 'thumb-down') : 'help'" 
				        			:type="posicion_en_debate != null ? (posicion_en_debate == 'F' ? 'is-success' : 'is-danger') : 'is-warning'" 
				        			size="is-small">
				        		</b-icon>
				                <span>Tú + Info 
				                	<b-tag size="is-small" rounded v-if="userLoged"> 
				                	{{ mis_opiniones.length }}
					                </b-tag>
					                <b-tag size="is-small" rounded v-else="userLoged">-</b-tag>
					            </span>
				            </template>

				        	<b-input type="textarea" v-if="editandoTituloDebate && es_propio_debate" @keydown.native.enter="saveTituloDebate()" v-model="debateSeleccionado().titulo"></b-input>

					    	<h1 class="title is-4" v-else="" @click.stop.prevent="es_propio_debate ? editTituloDebate() : null">{{  debateSeleccionado().titulo  }}</h1>

					    	<b-input type="textarea" v-if="editandoTextoDebate && es_propio_debate" @keydown.native.enter="saveTextoDebate()" v-model="debateSeleccionado().texto"></b-input>
					    	<span v-else="" @click.stop.prevent="es_propio_debate ? editTextoDebate() : null">{{  debateSeleccionado().texto  }}</span>

					    	<template v-if="userLoged && posicion_en_debate != null">
					    		<h1>Posicionado: {{ posicion_en_debate }}</h1>
						    	<h1>Mis Opiniones</h1>
						      	<ul class="list-group">
					    			<li class="list-group-item" v-for="opinion in mis_opiniones">
					    				<span>{{  opinion.id }}</span> -  
					    				<span>{{  opinion.texto }}</span>
					    			</li>
						    	</ul>
						    
						    	<h1>Responde:</h1>
						    	<b-input type="textarea" v-model="nueva_respuesta"></b-input>
						    	<br>
						    	<button class="btn btn-success" @click.stop.prevent="respondeDebate()">Responde Debate</button>
						    </template>
						    <template v-else-if="userLoged">
						    	<h1>Posicionate</h1>
						    	<b-button type="is-success" outlined @click.stop.prevent="clickPosicionate(id_debate, 'F')">A Favor</b-button>
				    			<b-button type="is-danger" outlined @click.stop.prevent="clickPosicionate(id_debate, 'C')">En Contra</b-button>
				    		</template>
				        </b-tab-item>
				            
				        <b-tab-item>
				            <template slot="header">
				                <b-icon icon="thumb-up" type="is-success" size="is-small"></b-icon>
				                <span> A Favor <b-tag rounded size="is-small"> {{ opiniones_favorables.length }} </b-tag> </span>
				            </template>
				            <h1>A Favor</h1>
					      	<ul class="list-group">
				    			<li class="list-group-item" v-for="opinion_favorable in opiniones_favorables">
				    				{{ '[' + opinion_favorable.id + '] ' + opinion_favorable.su_usuario + ' - ' + opinion_favorable.texto }}
				    			</li>
					    	</ul>

				        </b-tab-item>
				    </b-tabs>
				</template>	
		    </div>
		</div>

		<div class="columns" v-if="seccion==2">
			<div class="column is-paddingless">
				<div class="panel panel-default is-primary" v-if="userLoged">
					<div class="panel-heading">
				    	<h3 class="panel-title">Nuevo debate</h3>
				  	</div>
			  		<div class="px-5 py-5">
			    		<b-field  label="Título" label-position="on-border">
				            <b-input type="text" v-model="titulo_nuevo_debate" required></b-input>
				        </b-field>
			    		<b-field label="Descripción" label-position="on-border">
				            <b-input type="textarea" v-model="texto_nuevo_debate" required></b-input>
				        </b-field>
				        <b-field label="Fecha" label-position="on-border">
				            <b-datepicker
				                rounded
				                placeholder="Elige..."
				                icon="calendar-today"
				                :locale="locale"
				                :datepicker="{ showWeekNumber }">
				            </b-datepicker>
				        </b-field>
				        <template>
						    <section>
						        <b-field label="Selecciona Temas Asociados" label-position="on-border">
						            <b-taginput
						                v-model="tags"
						                :data="temas_filtrados"
						                autocomplete
						                ref="taginput"
						                icon="label"
						                placeholder="Selecciona Tema"
						                @typing="getFilteredTags">
						                <template slot-scope="props">
						                    <strong>{{props.option.id_tema}}</strong>: {{props.option.nombre_tema}}
						                </template>
						                <template slot="empty">
						                    No hay Temas
						                </template>
						                <template slot="selected" slot-scope="props">
						                    <b-tag
						                        v-for="(tag, index) in props.tags"
						                        :key="index"
						                        type="is-primary"
						                        rounded
						                        closable
						                        @close="$refs.taginput.removeTag(index, $event)">
						                        {{ tag.nombre_tema }}
						                    </b-tag>
						                </template>
						            </b-taginput>
						        </b-field>
						    </section>
						</template>
				        <b-button class="is-primary mt-5" @click.stop.prevent="putDebate()">Añadir Debate</b-button>
			  		</div>
				</div>
			</div>
			<div class="column">
				<template v-if="misDebates.length > 0">
					<h1 class="title is-5 has-text-centered">Si deseas modificar tus datos...</h1>
	                <b-field  label="Titular" label-position="on-border">
			            <b-input type="text" v-model="registro_usuario.titular_usuario"></b-input>
			        </b-field>
		    		<b-field label="Descripción" label-position="on-border">
			            <b-input type="textarea" v-model="registro_usuario.descripcion_usuario"></b-input>
			        </b-field>
			        <b-field class="file is-primary" label="Selecciona tu Imagen de Perfil" label-position="on-border">
		        		<b-upload v-model="file_foto" 
				        		  class="file-label" 
				        		  drag-drop 
				        		  @input="cambioFotoPerfil()" accept=".jpg,.gif,.png,.bmp">
						        	<section class="section">
					                    <div class="content has-text-centered">
					                    	<figure class="image">
											  <img class="is-rounded" :src="file_foto_url" />
											</figure>
					                </section>
				        </b-upload>
				    </b-field>
			        <b-button class="is-primary mt-5" @click.stop.prevent="completarDatosRegistro()">Actualizar</b-button>
				</template>
				<b-steps v-else=""
		            v-model="activeStep"
		            :animated="true"
		            :rounded="true"
		            :has-navigation="false">
		            <b-step-item step="1" label="Crear" :clickable=false>
		                <h1 class="title has-text-centered">Crear</h1>
		            </b-step-item>

		            <b-step-item step="2" label="Completar" :clickable=false>
		                <h1 class="title has-text-centered">Completa algunos Datos</h1>
		                <b-field  label="Titular" label-position="on-border">
				            <b-input type="text" v-model="registro_usuario.titular_usuario"></b-input>
				        </b-field>
			    		<b-field label="Descripción" label-position="on-border">
				            <b-input type="textarea" v-model="registro_usuario.descripcion_usuario"></b-input>
				        </b-field>
				        <b-button class="is-primary mt-5" @click.stop.prevent="completarDatosRegistro()">Enviar</b-button>
		            </b-step-item>

		            <b-step-item step="3" label="Primer Debate" :clickable=false>
		                <h1 class="title has-text-centered">Primer Debate</h1>
		                <h5 class="title is-5 has-text-centered">Añade tu primer debate, con tus argumentos y opiniones.</h1>
		            </b-step-item> 
		        </b-steps>

		        <ul>
					<li v-for="(debate, index) in misDebates">
						{{ debate.titulo}} {{ index }}
					</li>
				</ul>
			</div>
			
		</div>

		<div class="columns" v-if="seccion==3">
			<div class="column">
				<section class="hero">
				  <div class="hero-body">
				    <div class="container">
				      <h1 class="title">
				        Regístrate en Debatium
				      </h1>
				      <h2 class="subtitle">
				        Crea tu cuenta para poder posicionarte, dar tu opinión y crear tus propios debates. Ahora solo lo básico, más adelante ya podrás completar tu información.
				      </h2>
				    </div>
				  </div>
				</section>

				<b-steps
		            v-model="activeStep"
		            :animated="true"
		            :rounded="true"
		            :has-navigation="false">
		            <b-step-item step="1" label="Crear" >
		                <h1 class="title has-text-centered">Crear</h1>
		                Registro
		            </b-step-item>

		            <b-step-item step="2" label="Completar">
		                <h1 class="title has-text-centered">Completar</h1>
		                Completa algunos Datos.
		            </b-step-item>

		            <b-step-item step="3" label="Primer Debate">
		                <h1 class="title has-text-centered">Primer Debate</h1>
		                Tu Primer Debate.
		            </b-step-item> 
		        </b-steps>

			    <section>
			    	<b-notification
                    	class="mb-5"
			            type="is-danger is-light"
			            v-model="notificacionesActivas.errorRegistro">
			            <b>Por favor, corrija el(los) siguiente(s) error(es):</b>
					    <ul>
					      <li v-for="error in registro_usuario.errors">{{ error }}</li>
					    </ul>
			        </b-notification>
			        <b-notification
                    	class="mb-5"
			            type="is-success is-light"
			            v-model="notificacionesActivas.registroCorrecto">
			            <b>Se ha realizado correctamente el nuevo registro</b>
			        </b-notification>

			        <template v-if="!notificacionesActivas.registroCorrecto">
				        <b-field label="Nombre de Usuario" label-position="on-border">
				            <b-input v-model="registro_usuario.nombre" expanded></b-input>
				        </b-field>

				        <b-field label="Email" label-position="on-border">
				            <b-input v-model="registro_usuario.email" type="email" placeholder="nobody@nowhere.com" expanded></b-input>
				        </b-field>

				        <b-field label="Contraseña" label-position="on-border">
				            <b-input v-model="registro_usuario.password" type="password" expanded password-reveal></b-input>
				        </b-field>

				        <b-field>
				            <p class="control">
				                <button class="button is-primary"  @click.stop.prevent="sendRegistro()">
				                  Registrarse
				                </button>
				            </p>
				        </b-field>
				    </template>

			    </section>
			</div>
			<div class="column">	
		    </div>
		</div>
	</div>
</div>