<?php if ($this->config->item("estadoProyecto") == "DEV"):?>
	<script src="assets/js/vue2.6.js"></script>
<?php elseif ($this->config->item("estadoProyecto") == "PROD"): ?>
	<script src="assets/js/vue2.6.produccion.js"></script>
<?php endif; ?>
	<script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
	<script src="https://unpkg.com/buefy/dist/buefy.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue-scrollto"></script>
	<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
	
	<!--Load the required libraries - jQuery, Kendo, Babel and Vue-->
	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
	<script src="https://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js"></script>
  	<script src="https://unpkg.com/@progress/kendo-grid-vue-wrapper/dist/cdn/kendo-grid-vue-wrapper.js"></script>
  	<script src="https://unpkg.com/@progress/kendo-datasource-vue-wrapper/dist/cdn/kendo-datasource-vue-wrapper.js"></script>
  	<script src="https://unpkg.com/@progress/kendo-barcodes-vue-wrapper/dist/cdn/kendo-barcodes-vue-wrapper.js"></script>
  	<script src="https://unpkg.com/@progress/kendo-map-vue-wrapper/dist/cdn/kendo-map-vue-wrapper.js"></script>


  	

	<?= $js; ?>
	
</body>
</html>