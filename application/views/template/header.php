<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Debatium</title>
	<meta name="viewport" content="width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0">
	<base href="<?= base_url(); ?>">
	<link href="https://unpkg.com/nprogress@0.2.0/nprogress.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdn.materialdesignicons.com/5.3.45/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="https://unpkg.com/buefy/dist/buefy.min.css">

	<!--Load Kendo styles from the Kendo CDN service-->
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.913/styles/kendo.common.min.css"/>
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.913/styles/kendo.default.min.css"/>

	<?php // http://materialdesignicons.com/ ?>
	<?= $css ?>
</head>
<body>