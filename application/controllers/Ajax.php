<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

	private $usuario_logueado; // Ajax
	private $datos_entrada;
	private $permitido;

	public function __construct()
	{
		parent::__construct();
		$this->datos_entrada = json_decode(file_get_contents("php://input"), true);
		$this->load->model("mod_debates");
		$this->load->model("mod_respuestas");
		$this->load->model("mod_acceso"); 
		$this->load->model("mod_utilidades"); 

		if ($this->mod_acceso->isUserLoged())
		{
			$this->usuario_logueado = $this->mod_acceso->getUserId();
		}
		else if (isset($this->datos_entrada['acceso_libre']) && $this->datos_entrada['acceso_libre'] == true)
		{ /* Adelante */ }	
		else
		{
			$this->usuario_logueado = 0;
			echo json_encode(array("userLoged" => false));
			die();
		}
    }

    //////////////////////////////////////7

    public function index()
    { 
    	$datos = $this->mod_acceso->getDatosUsuario($this->usuario_logueado);

    	echo json_encode(array("userLoged" => true, 
    		                   "userName" => $datos['nombre'],
    		                   "userId" => $datos['id'],
    		               	   "userTipo" => $datos['tipo'],
    		               	   "userTitular" => $datos['titular'],
    		               	   "userDescripcion" => $datos['descripcion'],
    		               	   "userFoto" => $datos['foto_perfil'])
    					);
    }


   	/////////////////////////////////////////

   	public function login()
   	{
   		$identificacion = $this->mod_acceso->identificar($this->datos_entrada['user'], $this->datos_entrada['pass']);

   		if ($identificacion['id']  != 0)
   		{
   			$this->mod_acceso->putUserId($identificacion['id']);
   			echo json_encode(array("userLoged" => true, 
   								   "userName" => $identificacion['nombre'], 
   								   "userId" => $identificacion['id'],
   								   "userTipo" => $identificacion['tipo'],
   								   "userTitular" => $identificacion['titular'],
    		               	   	   "userDescripcion" => $identificacion['descripcion'],
    		               	   	   "userFoto" => $identificacion['foto_perfil']));
   		}
   		else
   		{	
   			echo json_encode(array("userLoged" => false));
   		}
   	}

   	/////////////////////////////////////////

   	public function logout()
   	{
   		$this->mod_acceso->logout();
   	}

   	/////////////////////////////////////////

   	public function nuevoRegistro()
   	{
   		$datos = $this->datos_entrada['datos_registro'];

   		$this->db->select("USUA_Id as id, USUA_Nombre as nombre, USUA_Email as email");
   		$this->db->from("usuarios");
   		$this->db->where("USUA_Nombre", $datos['nombre']);
   		$this->db->or_where("USUA_Email", $datos['email']);
   		$query = $this->db->get();

   		if ($query->num_rows() > 0)
   		{
   			if ($query->row_array()['nombre'] == $datos['nombre'])
   				$datos['errors'][] = "Usuario Ya Existente";

   			if ($query->row_array()['email'] == $datos['email'])
   				$datos['errors'][] = "Email Ya Existente";

   			$datos['success'] = false;
   		}
   		else
   		{
   			$data = array("USUA_Nombre" => $datos['nombre'],
   						  "USUA_Email" => $datos['email'], 
   						  "USUA_Pass" => md5($datos['password']),
   						  "USUA_Tipo" => 10);

   			$this->db->insert("usuarios", $data);
   			$datos['success'] = true;
   		}

   		echo json_encode($datos);
   	}

	///////////////////////////////////////////////////

	public function addDebate()
	{
		$resultado = $this->mod_debates->addDebate($this->datos_entrada['titulo'],
			                                       $this->datos_entrada['texto'],
			                                       $this->datos_entrada['temas'],
		 										   $this->usuario_logueado);

		echo json_encode($this->mod_debates->getDebates($this->usuario_logueado, 
														array(), 
														$resultado['id_debate']));
	}

	///////////////////////////////////////////////////


	public function editDebate()
	{
		$this->db->set(($this->datos_entrada['campo'] == 'titulo') ? "DEBA_Titulo" : "DEBA_Texto",  $this->datos_entrada['valor']);
		$this->db->where("DEBA_Id",  $this->datos_entrada['id_debate']);
		$this->db->update("debates");
	}

	//////////////////////////////////////////////////////

	public function getDebates()
	{
		$datos = json_decode(file_get_contents("php://input"), true);

		if (isset($this->datos_entrada['id'])) {
			echo json_encode($this->mod_debates->getDebates($this->usuario_logueado, array(), $this->datos_entrada['id']));
		}
		else if (isset($this->datos_entrada['modo'])) {
			$filtros = array();
			$filtros['modo'] = $this->datos_entrada['modo'];

			$filtros_combinados = explode(':', $filtros['modo']);

			if (count($filtros_combinados) > 1) 
			{
				$filtros['id_entidad'] = $this->datos_entrada['id_entidad'];
			}

			if (isset($this->datos_entrada['cantidad']))
			{
				$filtros['cantidad'] = $this->datos_entrada['cantidad'];
			}

			if (isset($this->datos_entrada['terminoBusqueda']))
			{
				$filtros['terminoBusqueda'] = $this->datos_entrada['terminoBusqueda'];
			}



			echo json_encode($this->mod_debates->getDebates($this->usuario_logueado, $filtros));
		} 
		else {
			echo json_encode($this->mod_debates->getDebates($this->usuario_logueado, array()));
		}
	}

	////////////////////////////////////////////////////////////

	public function getRespuestasDebate()
	{
		$datos = json_decode(file_get_contents("php://input"), true);

		$this->db->select("RESP_Id as id, RESP_Texto as texto, RESP_SuUsuario as su_usuario, RESP_SuDebate su_debate");
		$this->db->from("respuestas");
		$this->db->join('posicionado_debates', 'posicionado_debates.PODE_SuUsuario = respuestas.RESP_SuUsuario AND posicionado_debates.PODE_SuDebate = respuestas.RESP_SuDebate');
		$this->db->where("RESP_SuDebate", $this->datos_entrada['id_debate']);
		$this->db->where("PODE_Posicionamiento", 'F');
		// Si no hay logueado, será usuario 0, por tanto, todas
		$this->db->where("RESP_SuUsuario !=", $this->usuario_logueado);

		$favorables = $this->db->get()->result_array();

		$this->db->select("RESP_Id as id, RESP_Texto as texto, RESP_SuUsuario as su_usuario, RESP_SuDebate su_debate");
		$this->db->from("respuestas");
		$this->db->join('posicionado_debates', 'posicionado_debates.PODE_SuUsuario = respuestas.RESP_SuUsuario AND posicionado_debates.PODE_SuDebate = respuestas.RESP_SuDebate');
		$this->db->where("RESP_SuDebate", $this->datos_entrada['id_debate']);
		$this->db->where("PODE_Posicionamiento", 'C');
		// Si no hay logueado, será usuario 0, por tanto, todas
		$this->db->where("RESP_SuUsuario !=", $this->usuario_logueado);

		$contrarias = $this->db->get()->result_array();

		$this->db->select("RESP_Id as id, RESP_Texto as texto, RESP_SuUsuario as su_usuario, RESP_SuDebate su_debate");
		$this->db->from("respuestas");
		$this->db->where("RESP_SuDebate", $this->datos_entrada['id_debate']);
		// Si no hay logueado, será usuario 0, por tanto, NINGUNA
		$this->db->where("RESP_SuUsuario", $this->usuario_logueado);

		$propias = $this->db->get()->result_array();

		echo json_encode(array("a_favor" => $favorables,
						       "en_contra" => $contrarias,
						   	   "propias" => $propias));
	}

	// Ajax
	public function deleteDebate()
	{
		$this->db->where("DEBA_id", $this->datos_entrada['valor']);
		$this->db->delete("debates");
	}

	// Ajax
	public function posicionarDebate()
	{
		$data = array ("PODE_SuUsuario" => $this->usuario_logueado,
					   "PODE_SuDebate" => $this->datos_entrada['id_debate'],
					   "PODE_Posicionamiento" => $this->datos_entrada['posicion']);

		$this->db->insert('posicionado_debates', $data);

		echo json_encode(array('id_insertado' => $this->db->insert_id()));
	}

	// Ajax
	public function seguimientoDebate()
	{
		$datos = json_decode(file_get_contents("php://input"), true);

		if ($datos['estado'] == false)
		{
			$this->db->where("SUDE_SuUsuario", $this->usuario_logueado);
			$this->db->where("SUDE_SuDebate", $this->datos_entrada['id_debate']);
			$this->db->delete("suscritos_debates");
			echo json_encode(array('id_no_suscrito_debate' => $this->datos_entrada['id_debate']));
		}
		else
		{
			$data = array ("SUDE_SuUsuario" => $this->usuario_logueado,
					   "SUDE_SuDebate" => $this->datos_entrada['id_debate']);

			$this->db->insert('suscritos_debates', $data);
			echo json_encode(array('id_suscrito_debate' => $this->db->insert_id()));
		}
		
	}

	function nuevaRespuesta()
	{

		$datos = json_decode(file_get_contents("php://input"), true);

		$this->db->select("PODE_Posicionamiento as posicionamiento");
		$this->db->from("posicionado_debates");
		$this->db->where("PODE_SuDebate", $this->datos_entrada['su_debate']);
		$this->db->where("PODE_SuUsuario", $this->usuario_logueado);

		$query = $this->db->get();


		$posicion = '';
		if ($query->num_rows() > 0)
		{
			$posicion = $query->row()->posicionamiento;
		}

		if ($posicion != '')
		{
			$data = array(
			 "RESP_Texto" => $this->datos_entrada['texto'],
			 "RESP_SuUsuario" => $this->usuario_logueado,
			 "RESP_SuDebate" => $this->datos_entrada['su_debate']);
		
			$this->db->insert('respuestas', $data);

			echo json_encode(array("id_respuesta" => $this->db->insert_id(), 
				                   "id_usuario" => $this->usuario_logueado,
				                   "posicion" => $posicion,
				               	   "success" => true));
		}
		else
		{
			echo json_encode(array("success" => false));
		}

	}


	public function getTemas($cantidad = 50)
	{
		$this->db->select("TEMA_Id as id_tema, TEMA_Nombre as nombre_tema");
		$this->db->limit($cantidad);
		echo json_encode($this->db->get("temas")->result_array());
	}

	public function getTemasMasUsados($cantidad = 50)
	{
		$this->db->select("COUNT(TEMA_Id) as veces_usado_tema, TEMA_Nombre as nombre_tema, TEMA_Id as idTema");
		$this->db->join("temas_debates", "TEDE_SuTema = TEMA_Id");
		$this->db->group_by("TEMA_Id");
		$this->db->order_by("veces_usado_tema", "DESC");
		$this->db->limit(10);
		$resultado = $this->db->get("temas")->result_array();

		echo json_encode($resultado);
	}

	public function completarDatosRegistro()
	{
		$data = array("USUA_Titular" => $this->datos_entrada['datos_registro']['titular_usuario'],
					  "USUA_Descripcion" => $this->datos_entrada['datos_registro']['descripcion_usuario'],
					  "USUA_Tipo" => 11);
		
		$this->db->where("USUA_Id", $this->datos_entrada['id_usuario']);
		$this->db->update("usuarios", $data);

		echo json_encode(array("step" => 2, "userTipo" => 11));
	}


	public function datosUsuarioFoto()
	{
		$resultado['error'] = false;
		if ($_FILES && $_FILES['file']['size'] > 0)
        {
            // fucion propia usando clase codeigniter
            $resultado_upload = $this->mod_utilidades->subir_fichero('file','fotos_perfil', "jpg|png|gif|bmp");

            if  ($resultado_upload['fichero'] != -1)
            {
                // En caso de que se haya subido el fichero correctamente
                $file = $resultado_upload['fichero']['file_name'];
                $resultado['error'] = false;
            }
            else
            {
            	$resultado['error'] = true;
            	$resultado['msg'] = $resultado_upload['msg'] . ", " . $resultado_upload['error'];
            }
        }
        else
        {
            //Subimos la información a la base de datos para registrar el usuario
            $file = '';
        }

		$data = array("USUA_Titular" => $_POST['userTitular'],
					  "USUA_Descripcion" => $_POST['userDescripcion'],
					  "USUA_Foto" => $file);
		
		$this->db->where("USUA_Id", $this->usuario_logueado);
		$this->db->update("usuarios", $data);

		echo json_encode($resultado);
	}


	/////////////////////////////////////////////////////////////

	function random_color_part() {
	    return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
	}

	function random_color() {
	    return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
	}

}