<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pruebarouter extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->data['js'] = load_js([
      'comun',
      'axios.min',
      'pruebarouter',
    ]);
  
    $this->render();
  }
}