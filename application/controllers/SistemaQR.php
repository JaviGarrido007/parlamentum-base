<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SistemaQR extends MY_Controller 
{
	private $datos_entrada;

	public function __construct()
	{
		parent::__construct();
		$pdo = new PDO('mysql:host=' . $this->db->hostname . ';dbname=' . $this->db->database, 
			           $this->db->username, 
			           $this->db->password);

		$params = array('pdo' => $pdo);
		$this->load->library('Shortener', $params);
    }

	public function index()
	{
		$this->data['js'] = load_js([
			'comun',
			'axios.min',
			'sistema_qr',
		]);
		
		$this->render('sistema_qr');
	}

	//////////////////////////////////////////////////////

	// Genera la TAG con la IMG que llama al generador de QR

	/*
	public function generar_qr($id_url)
	{
		$url_generador = base_url() . "SistemaQR/qr/" . $id_url;
		echo '<img src="' . $url_generador .'" />';
	}
	*/

	// Este genera el QR real
	public function qr($codigo)
	{
		$url_local = base_url() . 'v/';
		header('Content-Type: image/gif');
		echo file_get_contents("https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=" . urlencode($url_local . $codigo));
	}

	//////////////////////

	public function verUrl($codigo = '')
	{
		$this->db->select("ACQR_Id as id, ACQR_Url as url, ACQR_CodigoShort as shortCode, ACQR_NumAccesos as num_accesos");
		$this->db->from("accesos_qr");
		$this->db->where("ACQR_CodigoShort", $codigo);

		$query = $this->db->get();

		if ($query->num_rows() == 1)
		{
			$this->db->where("ACQR_Id", $query->row()->id);
			$this->db->set("ACQR_NumAccesos", "ACQR_NumAccesos + 1", false);
			$this->db->update("accesos_qr");

			redirect($query->row()->url);	
		}
		else
		{
			echo "Se ha producido un error";
		}
	}

	///// AJAX //////
	public function getListaUrls()
	{
		$this->datos_entrada = json_decode(file_get_contents("php://input"), true);
		$this->db->select("ACQR_Id as id, ACQR_Url as url, ACQR_CodigoShort as shortCode, ACQR_NumAccesos as num_accesos");
		$this->db->from("accesos_qr");

		$datos = $this->db->get()->result_array();
		echo json_encode($datos);
	}

	public function addUrl()
	{
		$shortURL_Prefix = 'https://debatium.com/'; // with URL rewrite
		$this->datos_entrada = json_decode(file_get_contents("php://input"), true);

		try{
		    $shortCode = $this->shortener->urlToShortCode($this->datos_entrada['url']);
		}catch(Exception $e){
		    echo $e->getMessage();
		    $shortCode = '';
		}

		$data = array("ACQR_Url" => $this->datos_entrada['url'],
					  "ACQR_CodigoShort" => $shortCode,
					  "ACQR_NumAccesos" => 0);

		$this->db->insert("accesos_qr", $data);

		echo json_encode(array("id" => $this->db->insert_id(),
							   "url" => $this->datos_entrada['url'],
							   "shortCode" => $shortCode,
							   "num_accesos" => 0));
	}
}