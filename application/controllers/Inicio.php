<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("mod_inits");
		$this->mod_inits->dbInit();
		$this->data['js'] = load_js([
			'jquery.min'
		]);
	}

	public function index()
	{
		$this->data['css'] .= load_css('inicio');
		$this->data['js'] = load_js([
			'kendo.culture.es-ES',
			'kendo.messages.es-ES',
			'comun',
			'axios.min',
			'inicio'
		]);
		
		if ($this->input->get('permitido'))
		{
			$this->render();
		}
		else
		{
			$this->render('proximamente');
		}
	}

	public function cat($continuar = '')
	{
		$endPoint = "https://es.wikipedia.org/w/api.php";
		$params = [
		    "action" => "query",
		    "format" => "json",
		    "list" => "allcategories",
		    "aclimit" => 500
		];

		if ($continuar != '')
		{
			$params['accontinue'] = urldecode($continuar);
		}

		$url = $endPoint . "?" . http_build_query( $params );

		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$output = curl_exec( $ch );
		curl_close( $ch );

		$result = json_decode( $output, true );

		if (isset($result['continue']))
		{ 
			$url_continuar = base_url() . "Inicio/cat/" . urlencode($result['continue']['accontinue']);
			echo "<a href='" . $url_continuar . "'>" . $url_continuar . "</a><br>";	
		}

		foreach( $result["query"]["allcategories"] as $k => $v ) {
		    echo( $v["*"] . "\n" . "<br>");
		    $data = array("TEMA_Nombre" => $v["*"], 
						  "TEMA_Descripcion" => $v["*"]);

			$this->db->insert("temas", $data);
		}

		
	}

	function prueba_th()
	{
		$url = "http://192.168.1.38/iparralde/vJon/imagenes_documentos/26dedb4ca0d2ded5837434ed8a966657.png";
		$url_exploded = explode("/", $url);

		$keys = array_keys($url_exploded);
		$index_fin = end($keys);
		$url_exploded[$index_fin] = 'th_' . $url_exploded[$index_fin];

		// $url_exploded[end(array_keys($url_exploded))] = 'th_' . $url_exploded[end(array_keys($url_exploded))];
		echo join("/", $url_exploded);
	}
}